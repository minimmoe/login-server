package main

import (
	"crypto/sha1"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"minimmoe/src/loginprotocol2"
	"minimmoe/src/usercache"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"syscall"

	"time"

	"gitlab.com/minimmoe/gomariadb"
	"gitlab.com/minimmoe/restclient"
	"gitlab.com/minimmoe/settings"
	"gitlab.com/minimmoe/tlstcpserver"
	"golang.org/x/crypto/pbkdf2"
)

func main() {

	closeRESTUpdate := make(chan interface{})
	IsRunning := true
	var WaitGroup sync.WaitGroup
	jobQue := make(chan net.Conn)

	cmdArgs := getCommandlineArgs(os.Args)
	if _, ok := cmdArgs["--help"]; ok {
		printHelp()
		os.Exit(0)
	}

	HandlerCount := 1
	if _, ok := cmdArgs["--multi"]; ok {
		HandlerCount = getConnectionHandlerCount()
	}

	ServerConf := settings.NewSettings("configuration.json")
	checkEnvironmentOverloads(ServerConf)

	restClient := hookToREST(ServerConf)

	// update to rest server
	go func() {
		publicIP, err := restClient.RequestWhatsMyIP()
		if err != nil {
			fmt.Println(err.Error())
		}

		for {
			select {
			case <-closeRESTUpdate:
				fmt.Println("closing rest update")
				break
			case <-time.After(2 * time.Second):
				if err := restClient.InsertLoginHandler(
					ServerConf.MappedConfigs["loginserver"]["name"],
					publicIP,
					ServerConf.MappedConfigs["loginserver"]["port"],
				); err != nil {
					fmt.Println("failed to post rest data. " + err.Error())
					os.Exit(-1)
				}
			}
		}
	}()

	cachedUsers := usercache.NewUserCache()

	// make sure mariaDB is reachable
	if err := gomariadb.TestConnection(
		ServerConf.MappedConfigs["database"]["username"],
		ServerConf.MappedConfigs["database"]["password"],
		ServerConf.MappedConfigs["database"]["databasename"],
		ServerConf.MappedConfigs["database"]["ip"],
		ServerConf.MappedConfigs["database"]["port"],
	); err != nil {
		fmt.Println(err.Error())
		os.Exit(-1)
	}

	InLogin := tlstcpserver.TCPServerConf{}
	if err := InLogin.SetTLSCertifications(
		ServerConf.MappedConfigs["loginserver"]["cert"],
		ServerConf.MappedConfigs["loginserver"]["key"],
	); err != nil {

		fmt.Println(err.Error())
		os.Exit(-1)
	}
	if err := InLogin.TLSListen(
		ServerConf.MappedConfigs["loginserver"]["address"],
		ServerConf.MappedConfigs["loginserver"]["port"],
	); err != nil {

		fmt.Println(err.Error())
		os.Exit(-1)
	}

	fmt.Println("Listening on: " +
		ServerConf.MappedConfigs["loginserver"]["address"] +
		" " +
		ServerConf.MappedConfigs["loginserver"]["port"])

	WaitGroup.Add(1)
	go acceptLoop(&jobQue, &WaitGroup, &IsRunning, &InLogin)

	// init LoginHandlers
	for i := 0; i < HandlerCount; i++ {
		WaitGroup.Add(1)
		go handleLogin(&WaitGroup, &IsRunning, &jobQue, cachedUsers, restClient,
			ServerConf)
	}

	// Signal interrupt routine
	go func() {
		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
		<-sigs // blocking
		close(sigs)
		fmt.Println("Closedown sequence start...")

		IsRunning = false

		closeRESTUpdate <- 0

		// close listener first!
		if err := InLogin.Close(); err != nil {
			fmt.Println(err.Error())
		}

		close(jobQue)
		// close connections
		for len(jobQue) > 0 {
			conn := <-jobQue
			if err := conn.Close(); err != nil {
				fmt.Println(err.Error())
			}
		}
	}()

	WaitGroup.Wait()
}

func getCommandlineArgs(args []string) map[string]bool {
	cmla := make(map[string]bool)
	for _, element := range args[1:] {
		cmla[element] = true
	}
	return cmla
}

func printHelp() {
	helpString :=
		`Command line args:
	--help				Prints out this help
	--multi				Enables multiple simultaneous Connection handlers
`

	fmt.Println(helpString)
}

func getConnectionHandlerCount() int {
	// CPU cores to use for login handlers
	HandlerCount := runtime.NumCPU()
	if HandlerCount >= 4 {
		HandlerCount -= 3
	} else if HandlerCount >= 3 {
		HandlerCount -= 2
	} else if HandlerCount > 2 {
		HandlerCount--
	}
	return HandlerCount
}

func checkEnvironmentOverloads(serverConf *settings.Settings) {
	// check environments for docker values
	if envValue := os.Getenv("LOGINSERVERNAME"); len(envValue) > 0 {
		serverConf.MappedConfigs["loginserver"]["name"] = envValue
	}
	if envValue := os.Getenv("LOGINSERVERIP"); len(envValue) > 0 {
		serverConf.MappedConfigs["loginserver"]["ip"] = envValue
	}
	if envValue := os.Getenv("LOGINSERVERPORT"); len(envValue) > 0 {
		serverConf.MappedConfigs["loginserver"]["port"] = envValue
	}
	if envValue := os.Getenv("LOGINSERVERCERT"); len(envValue) > 0 {
		serverConf.MappedConfigs["loginserver"]["cert"] = envValue
	}
	if envValue := os.Getenv("LOGINSERVERKEY"); len(envValue) > 0 {
		serverConf.MappedConfigs["loginserver"]["key"] = envValue
	}

	if envValue := os.Getenv("RESTIP"); len(envValue) > 0 {
		serverConf.MappedConfigs["restserver"]["restserverip"] = envValue
	}
	if envValue := os.Getenv("RESTPORT"); len(envValue) > 0 {
		serverConf.MappedConfigs["restserver"]["restserverport"] = envValue
	}
	if envValue := os.Getenv("RESTPASSWORD"); len(envValue) > 0 {
		serverConf.MappedConfigs["restserver"]["password"] = envValue
	}
	if envValue := os.Getenv("PUBLICIP"); len(envValue) > 0 {
		serverConf.MappedConfigs["restserver"]["publicip"] = envValue
	}
	if envValue := os.Getenv("DBIP"); len(envValue) > 0 {
		serverConf.MappedConfigs["database"]["ip"] = envValue
	}
	if envValue := os.Getenv("DBPORT"); len(envValue) > 0 {
		serverConf.MappedConfigs["database"]["port"] = envValue
	}
	if envValue := os.Getenv("DBNAME"); len(envValue) > 0 {
		serverConf.MappedConfigs["database"]["databasename"] = envValue
	}
	if envValue := os.Getenv("DBUSER"); len(envValue) > 0 {
		serverConf.MappedConfigs["database"]["username"] = envValue
	}
	if envValue := os.Getenv("DBPASSWORD"); len(envValue) > 0 {
		serverConf.MappedConfigs["database"]["password"] = envValue
	}
}

func hookToREST(serverConf *settings.Settings) *restclient.RESTClient {

	restClient, err := restclient.NewRestClient(
		serverConf.MappedConfigs["restserver"]["restserverip"],
		serverConf.MappedConfigs["restserver"]["restserverport"],
		serverConf.MappedConfigs["restserver"]["password"],
	)
	if err != nil {
		fmt.Println("failed to init rest hook: " + err.Error())
		os.Exit(-1)
	}

	return restClient
}

func acceptLoop(
	connections *chan net.Conn,
	wg *sync.WaitGroup,
	isRunning *bool,
	login *tlstcpserver.TCPServerConf,
) {
	defer wg.Done()

	for *isRunning {
		conn, err := login.Accept()
		if err != nil {
			fmt.Println(err.Error())
		} else {
			fmt.Println("connected " + conn.RemoteAddr().String())
			*connections <- conn
		}
	}
	fmt.Println("login accept close")
}

func handleLogin(
	wg *sync.WaitGroup,
	isRunning *bool,
	jobQue *chan net.Conn,
	users *usercache.UserCache,
	rest *restclient.RESTClient,
	serverConf *settings.Settings,
) {
	defer wg.Done()

	for *isRunning {

		conn := <-*jobQue
		if conn != nil {

			loginProtocol := loginprotocol2.NewLoginHandler(512, 1)
			username, password, err := loginProtocol.ReadLogin(conn)
			if err != nil {
				fmt.Println(err.Error())
				if errorBytes, err := loginProtocol.ErrorMessage([]byte{0x04, 0x3C}); err != nil {
					fmt.Println(err.Error())
				} else {

					conn.Write(errorBytes)
				}
				conn.Close()
				continue
			}
			//[]byte{0x3C, 0xB3}
			err, hash := getPasswordHash(username, users, serverConf)
			if err != nil {
				fmt.Println(err.Error())
				if errorBytes, err := loginProtocol.ErrorMessage([]byte{0x3C, 0xB3}); err != nil {
					fmt.Println(err.Error())
				} else {
					conn.Write(errorBytes)
				}
				conn.Close()
				continue
			}

			if len(hash) > 0 {

				if bans, isBanned := isUserBanned(username, serverConf); isBanned {

					message := "Active Bans:\n"
					message = strings.Join(bans[:], "\n")

					errorBytes := loginProtocol.BannedMessage(message)
					conn.Write(errorBytes)
					conn.Close()
					continue
				}

				if comparePasswords(hash, password) {

					sessionToken, err := makeSessionToken(username)
					if err != nil {
						fmt.Println(err.Error())
						if errorBytes, err := loginProtocol.ErrorMessage([]byte{0x04, 0x3C}); err != nil {
							fmt.Println(err.Error())
						} else {
							conn.Write(errorBytes)
						}
						conn.Close()
						break
					}

					connectionHandler, err := getConnectionHandler(rest)
					if err != nil {
						fmt.Println(err.Error())
						if errorBytes, err := loginProtocol.ErrorMessage([]byte{0x9C, 0xEC}); err != nil {
							fmt.Println(err.Error())
						} else {

							conn.Write(errorBytes)
						}
						conn.Close()
						break
					}

					// Insert session token and username to rest server
					if err := rest.InsertLoginToken(
						username,
						sessionToken,
					); err != nil {
						fmt.Println("Insert to rest failed.\n" + err.Error())
						os.Exit(-1)
					}

					if okMessage, err := loginProtocol.OkMessage(
						sessionToken,
						connectionHandler["IP"],
						connectionHandler["Port"],
					); err != nil {
						fmt.Println(err.Error())
						if errorBytes, err := loginProtocol.ErrorMessage([]byte{0x04, 0x3C}); err != nil {
							fmt.Println(err.Error())
						} else {
							conn.Write(errorBytes)
						}
					} else {
						conn.Write(okMessage)
					}

				} else {

					// passwords did not match => generic error
					if errorBytes, err := loginProtocol.ErrorMessage([]byte{0x04, 0x3C}); err != nil {
						fmt.Println(err.Error())
					} else {
						conn.Write(errorBytes)
					}

				}
			} else {
				// username not found => generic error
				if errorBytes, err := loginProtocol.ErrorMessage([]byte{0x04, 0x3C}); err != nil {
					fmt.Println(err.Error())
				} else {
					conn.Write(errorBytes)
				}
			}

			conn.Close()
		}
	}
	fmt.Println("login handler closing")
}

func getPasswordHash(
	username string,
	users *usercache.UserCache,
	serverConf *settings.Settings,
) (error, string) {
	hash, isCached := users.GetValue(username)
	if !isCached {

		if err := findUserFromDatabase(serverConf, users,
			username,
		); err != nil {
			return errors.New("Could not find user or connect to database"), ""
		}
		hash, _ = users.GetValue(username)
	}
	return nil, hash
}

func findUserFromDatabase(
	ServerConf *settings.Settings,
	cache *usercache.UserCache,
	username string,
) error {

	databaseConnection := gomariadb.MariaDBConf{}
	err := databaseConnection.Connect(
		ServerConf.MappedConfigs["database"]["username"],
		ServerConf.MappedConfigs["database"]["password"],
		ServerConf.MappedConfigs["database"]["databasename"],
		ServerConf.MappedConfigs["database"]["ip"],
		ServerConf.MappedConfigs["database"]["port"],
	)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(-1)
	}
	defer databaseConnection.Close()

	// get username password hash pairs
	hash, err := databaseConnection.GetHash(username)
	if err != nil {
		return err
	}

	cache.SetValue(username, hash)

	return nil
}

func comparePasswords(hash, password string) bool {

	iterations, salt, passwordHash, err := splitHash(hash)
	if err != nil {
		fmt.Println(err.Error())
	}

	givenPasswordHashed := pbkdf2.Key([]byte(password),
		[]byte(salt),
		iterations,
		32,
		sha256.New,
	)
	return passwordHash == base64.StdEncoding.EncodeToString(givenPasswordHashed)
}

func splitHash(hash string) (int, string, string, error) {

	hashParts := strings.Split(hash, "$")
	iterations, err := strconv.Atoi(hashParts[1])
	if err != nil {
		return 0, "", "", err
	}
	salt := hashParts[2]
	passwordHash := hashParts[3]
	return iterations, salt, passwordHash, nil
}

func makeSessionToken(username string) (string, error) {
	tokenString := username + strconv.FormatInt(time.Now().UnixNano(), 10)
	hasher := sha1.New()
	if _, err := hasher.Write([]byte(tokenString)); err != nil {
		return "", err
	}
	return hex.EncodeToString(hasher.Sum(nil)), nil
}

func getConnectionHandler(RestClient *restclient.RESTClient) (map[string]string,
	error,
) {

	jsonString, err := RestClient.RequestLeastCrowdedConnectionHandler()
	if err != nil {
		fmt.Println(err.Error())
	}

	jsonMap := make(map[string]string)
	err = json.Unmarshal([]byte(jsonString), &jsonMap)
	if err != nil {
		return nil, err
	}

	return jsonMap, nil
}

func isUserBanned(
	username string,
	ServerConf *settings.Settings,
) ([]string, bool) {

	databaseConnection := gomariadb.MariaDBConf{}
	err := databaseConnection.Connect(
		ServerConf.MappedConfigs["database"]["username"],
		ServerConf.MappedConfigs["database"]["password"],
		ServerConf.MappedConfigs["database"]["databasename"],
		ServerConf.MappedConfigs["database"]["ip"],
		ServerConf.MappedConfigs["database"]["port"],
	)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(-1)
	}
	defer databaseConnection.Close()

	// get username password hash pairs
	bans, err := databaseConnection.GetBans(username)
	if err != nil {
		return nil, false
	}

	if bans == nil {
		return nil, false
	}
	return bans, true
}
