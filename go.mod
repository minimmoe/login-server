module minimmoe

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/mediocregopher/radix/v3 v3.4.2 // indirect
	gitlab.com/minimmoe/confgetter v0.0.0-20200123112047-5a3850bb0526
	gitlab.com/minimmoe/gomariadb v0.0.0-20200221013907-216c7d648055
	gitlab.com/minimmoe/goredis v0.0.0-20190923171214-adfe57b497ad
	gitlab.com/minimmoe/goresthookcomponent v0.0.0-20200123105931-0bf26e1c5847
	gitlab.com/minimmoe/restclient v0.0.0-20200226154618-e5c2a0928bed
	gitlab.com/minimmoe/settings v0.0.0-20200123112047-5a3850bb0526
	gitlab.com/minimmoe/tlstcpserver v0.0.0-20200120163521-c9c2b60fbe28
	golang.org/x/crypto v0.0.0-20200221231518-2aa609cf4a9d
)
