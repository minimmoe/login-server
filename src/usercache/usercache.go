package usercache

import "sync"

// UserCache stores a map coupled with mutex.
type UserCache struct {
	cachedUsers map[string]string
	mux         sync.Mutex
}

// NewUserCache is a UserCache type constructor
func NewUserCache() *UserCache {
	cache := new(UserCache)
	cache.cachedUsers = make(map[string]string)
	return cache
}

// GetValue returns a value from given key inside a map. Threadsafe.
func (U *UserCache) GetValue(key string) (string, bool) {
	U.getLock()
	returnValue, ok := U.cachedUsers[key]
	U.releaseLock()
	return returnValue, ok
}

// SetValue sets a value with given key to the map. Threadsafe.
func (U *UserCache) SetValue(key string, value string) {
	U.getLock()
	U.cachedUsers[key] = value
	U.releaseLock()
}

// getLock secures the usercache for concurrent operation
func (U *UserCache) getLock() {
	U.mux.Lock()
}

// releaseLock relases the usercache mutex for next concurrent operation
func (U *UserCache) releaseLock() {
	U.mux.Unlock()
}
