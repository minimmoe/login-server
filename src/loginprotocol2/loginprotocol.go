// Package loginprotocol2 is used by login server to read and format bytes so
// that server - client authentication communication is possible.
package loginprotocol2

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"net"
	"regexp"
	"time"
)

// LoginHandler implements server side protocol found here:
// gitlab.com/minimmoe/docs/blob/master/Login-Handler-Protocol.txt
type LoginHandler struct {
	ReadBytesBuffer []byte
	TimeoutTime     time.Duration
	TempBufferSize  int
}

// NewLoginHandler is a default constructor for LoginHandler type.
func NewLoginHandler(bufferSize int, timeoutTime time.Duration) *LoginHandler {

	lh := new(LoginHandler)
	lh.TimeoutTime = timeoutTime * time.Second
	lh.TempBufferSize = bufferSize
	return lh
}

// ReadLogin reads bytes from given connection until it can return username and
// password
func (L *LoginHandler) ReadLogin(conn net.Conn) (string, string, error) {

	L.ReadBytesBuffer = make([]byte, 0) // clear old buffers

	// make sure read can never hang.
	if err := conn.SetReadDeadline(time.Now().Add(L.TimeoutTime)); err != nil {
		return "", "", err
	}

	if err := L.checkIDBytes(conn, []byte{0x97, 0x72, 0xA8, 0x9A, 0x5B}); err != nil {
		return "", "", errors.New(err.Error() + ": Protocol")
	}

	if err := L.checkIDBytes(conn, []byte{0x61, 0x7A}); err != nil {
		return "", "", errors.New(err.Error() + ": Log In")
	}

	// get name length
	if len(L.ReadBytesBuffer) < 1 {
		if err := L.readConnection(conn, 1); err != nil {
			return "", "", err
		}
	}
	usernameLength := uint8(L.ReadBytesBuffer[0])
	L.ReadBytesBuffer = L.ReadBytesBuffer[1:]

	// get name
	if len(L.ReadBytesBuffer) < int(usernameLength) {
		if err := L.readConnection(conn, int(usernameLength)-len(L.ReadBytesBuffer)); err != nil {
			return "", "", err
		}
	}
	username := string(L.ReadBytesBuffer[:usernameLength])
	L.ReadBytesBuffer = L.ReadBytesBuffer[usernameLength:]

	// get password length
	if len(L.ReadBytesBuffer) < 1 {
		if err := L.readConnection(conn, 1); err != nil {
			return "", "", err
		}
	}
	passwordLength := uint8(L.ReadBytesBuffer[0])
	L.ReadBytesBuffer = L.ReadBytesBuffer[1:]

	// get password
	if len(L.ReadBytesBuffer) < int(passwordLength) {
		if err := L.readConnection(conn, int(passwordLength)-len(L.ReadBytesBuffer)); err != nil {
			return "", "", err
		}
	}
	password := string(L.ReadBytesBuffer[:passwordLength])

	return username, password, nil
}

// reads connection and places message to LoginHandler ReadBytesBuffer.
// If ID is found, clears out all bytes from buffer up to and including ID from
// buffer leaving with only the message and return succesfully
func (L *LoginHandler) checkIDBytes(conn net.Conn, ID []byte) error {

	for i := 0; i < 10; i++ {

		if bytes.Contains(L.ReadBytesBuffer, ID) {

			L.ReadBytesBuffer = bytes.Split(L.ReadBytesBuffer, ID)[1]
			return nil
		}
		// error in this case does not matter, it just means that no ID was
		// found from the byte stream.
		L.readConnection(conn, L.TempBufferSize)
	}

	return errors.New("Could not find ID")
}

// Reads more bytes to buffer from given connection
func (L *LoginHandler) readConnection(conn net.Conn, count int) error {

	tmp := make([]byte, count)
	_, err := conn.Read(tmp)
	if err != nil {
		return err
	}
	L.ReadBytesBuffer = append(L.ReadBytesBuffer, tmp...)

	return nil
}

// OkMessage formats given port and ip strings to proper "Ok" message that the
// protocol understands.
func (L *LoginHandler) OkMessage(token, ip, port string) ([]byte, error) {

	if match, _ := regexp.MatchString(`\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b`, ip); match == false {
		return nil, errors.New("OkMessage received invalid IP string")
	}

	if match, _ := regexp.MatchString(`^()([1-9]|[1-5]?[0-9]{2,4}|6[1-4][0-9]{3}|65[1-4][0-9]{2}|655[1-2][0-9]|6553[1-5])$`, port); match == false {
		return nil, errors.New("OkMessage received invalid port string")
	}

	loginProtocolID := []byte{0x97, 0x72, 0xA8, 0x9A, 0x5B}
	okID := []byte{0x14, 0x4A}
	tokenLength := byte(uint8(len(token)))
	tokenBytes := []byte(token)
	ipLength := byte(uint8(len(ip)))
	ipBytes := []byte(ip)
	portLength := byte(uint8(len(port)))
	portBytes := []byte(port)

	OkMessage := append(loginProtocolID, okID...)
	OkMessage = append(OkMessage, tokenLength)
	OkMessage = append(OkMessage, tokenBytes...)
	OkMessage = append(OkMessage, ipLength)
	OkMessage = append(OkMessage, ipBytes...)
	OkMessage = append(OkMessage, portLength)
	OkMessage = append(OkMessage, portBytes...)

	return OkMessage, nil
}

// BannedMessage formats given reason to proper "Banned" message that the
// protocol understands.
func (L *LoginHandler) BannedMessage(reason string) []byte {

	loginProtocolID := []byte{0x97, 0x72, 0xA8, 0x9A, 0x5B}
	bannedID := []byte{0xA3, 0xDB}
	reaslen := make([]byte, 2)
	binary.BigEndian.PutUint16(reaslen, uint16(len(reason)))
	banReasonLength := reaslen
	banReasonBytes := []byte(reason)

	bannedMessage := append(loginProtocolID, bannedID...)
	bannedMessage = append(bannedMessage, banReasonLength...)
	bannedMessage = append(bannedMessage, banReasonBytes...)

	fmt.Println(string(bannedMessage))
	return bannedMessage
}

// ErrorMessage formats given error cde to proper "error" message that the
// protocol understands.
func (L *LoginHandler) ErrorMessage(errCode []byte) ([]byte, error) {

	if len(errCode) != 2 {
		return nil, errors.New("errorMessage received bad error code")
	}

	loginProtocolID := []byte{0x97, 0x72, 0xA8, 0x9A, 0x5B}
	errorMessageID := []byte{0x21, 0x32}

	errorMessage := append(loginProtocolID, errorMessageID...)
	errorMessage = append(errorMessage, errCode[0])
	errorMessage = append(errorMessage, errCode[1])

	return errorMessage, nil
}
